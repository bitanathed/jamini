;(function() {

	'use strict';
	function classReg( className ) {
		return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
	  }
	  
	  var hasClass, addClass, removeClass;
	  
	  if ( 'classList' in document.documentElement ) {
		hasClass = function( elem, c ) {
		  return elem.classList.contains( c );
		};
		addClass = function( elem, c ) {
		  elem.classList.add( c );
		};
		removeClass = function( elem, c ) {
		  elem.classList.remove( c );
		};
	  }
	  else {
		hasClass = function( elem, c ) {
		  return classReg( c ).test( elem.className );
		};
		addClass = function( elem, c ) {
		  if ( !hasClass( elem, c ) ) {
			elem.className = elem.className + ' ' + c;
		  }
		};
		removeClass = function( elem, c ) {
		  elem.className = elem.className.replace( classReg( c ), ' ' );
		};
	  }
	  
	  function toggleClass( elem, c ) {
		var fn = hasClass( elem, c ) ? removeClass : addClass;
		fn( elem, c );
	  }
	  
	  var $ = {
		hasClass: hasClass,
		addClass: addClass,
		removeClass: removeClass,
		toggleClass: toggleClass,
		has: hasClass,
		add: addClass,
		remove: removeClass,
		toggle: toggleClass
	  };
	  
	  // transport
	  if ( typeof define === 'function' && define.amd ) {
		// AMD
		define( $ );
	  } else if ( typeof exports === 'object' ) {
		// CommonJS
		module.exports = $;
	  } else {
		// browser global
		window.$ = $;
	  }
	
		// from http://stackoverflow.com/a/25273333
	var bezier = function(x1, y1, x2, y2, epsilon) {
			var curveX = function(t){
				var v = 1 - t;
				return 3 * v * v * t * x1 + 3 * v * t * t * x2 + t * t * t;
			};
			var curveY = function(t){
				var v = 1 - t;
				return 3 * v * v * t * y1 + 3 * v * t * t * y2 + t * t * t;
			};
			var derivativeCurveX = function(t){
				var v = 1 - t;
				return 3 * (2 * (t - 1) * t + v * v) * x1 + 3 * (- t * t * t + 2 * v * t) * x2;
			};
			return function(t){
				var x = t, t0, t1, t2, x2, d2, i;
				// First try a few iterations of Newton's method -- normally very fast.
				for (t2 = x, i = 0; i < 8; i++){
					x2 = curveX(t2) - x;
					if (Math.abs(x2) < epsilon) return curveY(t2);
					d2 = derivativeCurveX(t2);
					if (Math.abs(d2) < 1e-6) break;
					t2 = t2 - x2 / d2;
				}

				t0 = 0, t1 = 1, t2 = x;

				if (t2 < t0) return curveY(t0);
				if (t2 > t1) return curveY(t1);

				// Fallback to the bisection method for reliability.
				while (t0 < t1){
					x2 = curveX(t2);
					if (Math.abs(x2 - x) < epsilon) return curveY(t2);
					if (x > x2) t0 = t2;
					else t1 = t2;
					t2 = (t1 - t0) * .5 + t0;
				}
				// Failure
				return curveY(t2);
			};
		},
		getRandomNumber = function(min, max) {
			return Math.floor(Math.random() * (max - min + 1)) + min;
		},
		throttle = function(fn, delay) {
			var allowSample = true;

			return function(e) {
				if (allowSample) {
					allowSample = false;
					setTimeout(function() { allowSample = true; }, delay);
					fn(e);
				}
			};
		},
		prefix = (function () {
			var styles = window.getComputedStyle(document.documentElement, ''),
				pre = (Array.prototype.slice.call(styles).join('').match(/-(moz|webkit|ms)-/) || (styles.OLink === '' && ['', 'o']))[1],
				dom = ('WebKit|Moz|MS|O').match(new RegExp('(' + pre + ')', 'i'))[1];
			
			return {
				dom: dom,
				lowercase: pre,
				css: '-' + pre + '-',
				js: pre[0].toUpperCase() + pre.substr(1)
			};
		})();
	
	var support = {transitions : Modernizr.csstransitions},
		transEndEventNames = { 'WebkitTransition': 'webkitTransitionEnd', 'MozTransition': 'transitionend', 'OTransition': 'oTransitionEnd', 'msTransition': 'MSTransitionEnd', 'transition': 'transitionend' },
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		onEndTransition = function( el, callback, propTest ) {
			var onEndCallbackFn = function( ev ) {
				if( support.transitions ) {
					if( ev.target != this || propTest && ev.propertyName !== propTest && ev.propertyName !== prefix.css + propTest ) return;
					this.removeEventListener( transEndEventName, onEndCallbackFn );
				}
				if( callback && typeof callback === 'function' ) { callback.call(this); }
			};
			if( support.transitions ) {
				el.addEventListener( transEndEventName, onEndCallbackFn );
			}
			else {
				onEndCallbackFn();
			}
		},
		// the main component element/wrapper
		component = document.querySelector('.component'),
		// the initial button
		buttonStart = component.querySelector('button.button--start'),
		// the listen button
		buttonListen = component.querySelector('.morph__button'),
		//the tag list
		tagList = document.querySelector('.context'),
		//the download
		download = document.querySelector('.player__control.icon--download'),
		//the refresh
		refresh = document.querySelector('.player__control.icon--refresh'),
		//the Github
		github = document.querySelector('.player__control.icon--github'),

		buttonListenContent = component.querySelector('.button__content.button__content--listen'),
		svgElement = component.querySelector('svg.morpher'),
		snap = Snap(svgElement),
		svgPath = snap.select('path'),
		totalNotes = 50,
		notes,
		notesSpeedFactor = 4.5,
		simulateTime = 26500,
		winsize = {width: window.innerWidth, height: window.innerHeight},
		buttonStartOffset = buttonStart.getBoundingClientRect(),
		buttonStartSize = {width: buttonStart.offsetWidth, height: buttonStart.offsetHeight},
		isListening = false,
		contentEl = component.querySelector('.player'),
		contentClose = contentEl.querySelector('.button--close');

	function init() {
		createNotes();
		initEvents();
	}


	function createNotes() {
		var notesEl = document.createElement('div'), notesElContent = '';
		notesEl.className = 'notes';
		for(var i = 0; i < totalNotes; ++i) {
			var j = (i + 1) - 6 * Math.floor(i/6);
			notesElContent += '<div class="note icon icon--note' + j + '"></div>';
		}
		notesEl.innerHTML = notesElContent;
		component.insertBefore(notesEl, component.firstChild)

		notes = [].slice.call(notesEl.querySelectorAll('.note'));
	}


	function initEvents() {
		buttonStart.addEventListener('click', listen);
		buttonListen.addEventListener('click',process);
		download.addEventListener('click',downloader);
		refresh.addEventListener('click',refresher);
		github.addEventListener('click',gitter);
		contentClose.addEventListener('click', closePlayer);
		window.addEventListener('resize', throttle(function(ev) {
			winsize = {width: window.innerWidth, height: window.innerHeight};
			buttonStartOffset = buttonStart.getBoundingClientRect();
		}, 10));
	}

	function downloader(){
		const a = document.createElement('a')
		const text = document.querySelector('.song__title').innerText + '\n' + document.querySelector('.song__details').innerText + '\n' + document.querySelector('.song__lyrics').innerText;
		a.style.position = "absolute"
		a.style.left = "-9999px;"
		a.download = "lyrics.txt"
		a.setAttribute('href', 'data:text/html;charset=utf-8, ' + encodeURIComponent(text));
		document.body.appendChild(a)
		a.click();
		document.body.removeChild(a)
	}

	function refresher(){
		closePlayer();
		isListening = true;
		setTimeout(time=>{
			$.remove(buttonStart, 'button--start');
			$.add(buttonStart, 'button--listen');
			buttonListenContent.innerHTML = `<span class="icon icon--ellipsis"></span>`

			animatePath(svgPath, component.getAttribute('data-path-listen'), 400, [0.8, -0.6, 0.2, 1], function() {
				$.add(buttonStart, 'button--animate');
				showNotes();
			});

			let blob = window.soundblob
			const reader = new FileReader();
				
			reader.readAsDataURL(blob); 
			reader.onloadend = async function() {
				let base64data = reader.result;                
				let prompt = tags();
				let results = await generate(prompt,base64data);
				if(!!results){
					layout(results)
				}
				stopListening();
			}

		},100);
	
		
	}

	function gitter(){
		window.open("https://gitlab.com/bitanathed/jamini","_blank")
	}

	function process(){

		window.stopRecording();
		
		buttonListenContent.innerHTML = `<span class="icon icon--ellipsis"></span>`

		const reader = new FileReader();

		setTimeout(async time=>{
			// window.soundblob = await fetch("testaudio.wav").then(response => response.blob())

			let blob = window.soundblob
			
			reader.readAsDataURL(blob); 
			reader.onloadend = async function() {
				let base64data = reader.result;                
				let prompt = tags();
				let results = await generate(prompt,base64data);
				if(!!results){
					layout(results)
				}
				stopListening();
			}
		},200); 
	}

	function layout(json){
		console.log("Laying out ",json)
		const song_title = document.querySelector(".song__title")
		const song_details = document.querySelector(".song__details-description")
		const song_lyrics = document.querySelector(".song__lyrics")

		song_title.innerText = json.title || "Song Title"
		song_details.innerText = json.description || "Song Description"
		song_lyrics.innerHTML = ""
		json.lyrics.forEach((lyric,i)=>{
			let p = `<p>${lyric.replaceAll("[","<sup>").replaceAll("]","</sup>")}</p>`
			song_lyrics.innerHTML += p
		})
	}

	async function generate(prompt,audioB64){
		try{
			let part1 = {text: prompt}
			let part2 = {inline_data: { data: audioB64.replace("data:audio/wav;base64,",""), mime_type: "audio/wav" }}
			let part3 = {parts: [{text: "You are an assistant for songwriting. Your response must be a pure JSON stringified object with no markdown operators having the following schema.\ntitle: The string title of the song \n description: The string description of the song \n lyrics: array of lyrics with the key or scale in brackets"}]}
			let part4 = beSafe("BLOCK_NONE")
			
			let obj = {
				"contents":[ { "parts":[ part1, part2 ] } ],
				"systemInstruction": part3,
				"safetySettings": part4
			}
			console.log("Got base 64 data now to query",part1);
			const geminiResponse = await gemini(obj);
			console.log(JSON.stringify(geminiResponse))
			const textResponse = geminiResponse.candidates[0].content.parts[0].text
			const jsonResponse = JSON.parse(textResponse.replace("```json","").replace("```",""))
			return jsonResponse
		}catch(e){
			console.log(e)
			alert("Errored out while fetching Gemini")
			setTimeout(e=>{
				window.location.reload();
			},1500);
			return false
		}
		
	}


	async function gemini(contents,url = "https://generativelanguage.googleapis.com/v1beta/models/gemini-1.5-pro-latest:generateContent", data = {}) {
		const response = await fetch(url+`?key=${"your-key-here"}`, {
		  method: "POST", 
		  mode: "cors", 
		  cache: "no-cache", 
		  headers: {
			"Content-Type": "application/json",
			"Accept": "application/json"
		  },
		  redirect: "follow", 
		  body: JSON.stringify(contents)
		});
		return response.json();
	}

	function tags(){
		const categories = ["genre","scale","beats","theme","mood"]
		let prompt = "Complete the song below. Add lyrics "
		categories.forEach(category=>{
			let node = document.querySelector(`.tag-${category}`)
			if(!!node){
				if(!!node.innerText && node.innerText.toLowerCase().indexOf("any") < 0){
					prompt += `with ${category} of ${node.innerText.replace("#","")},`
				}
			}
		})
		console.log("Prompt ",prompt)
		let tagText = document.getElementById("tag").innerText;
		if(!!tagText && tagText.toLowerCase().indexOf("add your own") < 0){
			tagText = tagText.replace(/[<>{}\+;]/g, ''); //sanitize text
		}else{
			tagText = ""
		}

		console.log("Additional text ",tagText)
		return prompt + tagText + "\n"
	}

	function listen() {
		window.checkPermissions(granted=>{
			if(granted){
				window.setupRecorderToRecord(recording=>{
					isListening = true;
					$.remove(buttonStart, 'button--start');
					$.add(buttonStart, 'button--listen');

					buttonListenContent.innerHTML = `<span class="icon icon--stop"></span>`

					// animate the shape of the button (we are using Snap.svg for this)
					animatePath(svgPath, component.getAttribute('data-path-listen'), 400, [0.8, -0.6, 0.2, 1], function() {
						// ripples start...
						$.add(buttonStart, 'button--animate');
						// music notes animation starts...
						showNotes();
					});

				});
			}
		});
		
	}

	
	function stopListening() {
		isListening = false;
		$.add(tagList,"hidden");
		$.remove(buttonStart, 'button--animate');
		hideNotes();
		showPlayer();
		buttonListenContent.innerHTML = `<span class="icon icon--microphone"></span>`
	}

	function showNotes() {
		notes.forEach(function(note) {
			positionNote(note);
			animateNote(note);
		});
	}

	function hideNotes() {
		notes.forEach(function(note) {
			note.style.opacity = 0;
		});
	}

	function positionNote(note) {
		// we want to position the notes randomly (translation and rotation) outside of the viewport
		var x = getRandomNumber(-2*(buttonStartOffset.left + buttonStartSize.width/2), 2*(winsize.width - (buttonStartOffset.left + buttonStartSize.width/2))), y,
			rotation = getRandomNumber(-30, 30);

		if( x > -1*(buttonStartOffset.top + buttonStartSize.height/2) && x < buttonStartOffset.top + buttonStartSize.height/2 ) {
			y = getRandomNumber(0,1) > 0 ? getRandomNumber(-2*(buttonStartOffset.top + buttonStartSize.height/2), -1*(buttonStartOffset.top + buttonStartSize.height/2)) : getRandomNumber(winsize.height - (buttonStartOffset.top + buttonStartSize.height/2), winsize.height + winsize.height - (buttonStartOffset.top + buttonStartSize.height/2));
		}
		else {
			y = getRandomNumber(-2*(buttonStartOffset.top + buttonStartSize.height/2), winsize.height + winsize.height - (buttonStartOffset.top + buttonStartSize.height/2));
		}

		note.style.WebkitTransition = note.style.transition = 'none';
		
		note.style.WebkitTransform = note.style.transform = 'translate3d(' + x + 'px,' + y + 'px,0) rotate3d(0,0,1,' + rotation + 'deg)';

		note.setAttribute('data-tx', Math.abs(x));
		note.setAttribute('data-ty', Math.abs(y));
	}


	function animateNote(note) {
		setTimeout(function() {
			if(!isListening) return;
			var noteSpeed = notesSpeedFactor * Math.sqrt(Math.pow(note.getAttribute('data-tx'),2) + Math.pow(note.getAttribute('data-ty'),2));

			note.style.WebkitTransition = '-webkit-transform ' + noteSpeed + 'ms ease, opacity 0.8s';
			note.style.transition = 'transform ' + noteSpeed + 'ms ease-in, opacity 0.8s';
			
			note.style.WebkitTransform = note.style.transform = 'translate3d(0,0,0)';
			note.style.opacity = 1;
			
			var onEndTransitionCallback = function() {
				note.style.WebkitTransition = note.style.transition = 'none';
				note.style.opacity = 0;

				if(!isListening) return;

				positionNote(note);
				animateNote(note);
			};

			onEndTransition(note, onEndTransitionCallback, 'transform');
		}, 60);
	}

	function showPlayer() {
		setTimeout(function() {
			animatePath(svgPath, component.getAttribute('data-path-player'), 450, [0.7, 0, 0.3, 1], function() {
				$.remove(contentEl, 'player--hidden');
			});
			$.add(buttonStart, 'button--hidden');
		}, 250);
		$.remove(buttonStart, 'button--listen');
	}

	function closePlayer() {
		$.add(contentEl, 'player--hidden');
		$.remove(tagList,"hidden");
		animatePath(svgPath, component.getAttribute('data-path-start'), 400, [0.4, 1, 0.3, 1]);
		setTimeout(function() {
			$.remove(buttonStart, 'button--hidden');
			$.add(buttonStart, 'button--start');
		}, 50);
	}

	function animatePath(el, path, duration, timingFunction, callback) {
		var epsilon = (1000 / 60 / duration) / 4,
			timingFunction = typeof timingFunction == 'function' ? timingFunction : bezier(timingFunction[0], timingFunction[1], timingFunction[2], timingFunction[3], epsilon);

		el.stop().animate({'path' : path}, duration, timingFunction, callback);
	}

	init();

})();