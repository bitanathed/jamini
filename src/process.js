function changeTag(self,category){
    
    document.querySelector(`.tag-${category}`).classList.remove(`tag-${category}`)
    self.childNodes[0].classList.add(`tag-${category}`)
}

function addTag(){
    document.querySelector(".tag[contenteditable]").classList.add("tag-editable")
}

function checkPermissions(cb){
    navigator.permissions.query(
        { name: 'microphone' }
    ).then(function(permissionStatus){
        console.log("Microphone permissions",permissionStatus.state);
        if(permissionStatus.state == "denied"){
            alert("Microphone permissions not granted. Please go to Settings to change. The app will not work as intended.")
            return cb(false)
        }
        return cb(true)
    })
}

function setupRecorderToRecord(cb){
    if(!!!window.recorder){
        navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => {
            window.chunks = [];
            
            window.recorder = new MediaRecorder(stream);
            
            window.recorder.ondataavailable = e => {
                console.log("data available")
                window.chunks.push(e.data);
                if (window.recorder.state == 'inactive') {
                    window.soundblob = new Blob(chunks, { type: 'audio/wav' });
                }
            };

            window.startRecording();
            return cb(true)
        }).catch(e=>{
            alert("Microphone permissions not granted. Please go to Settings to change. The app will not work as intended.")
        });
    }else{
        window.chunks = [];
        window.soundblob = null;
        window.startRecording();
        return cb(true)
    }
}

function startRecording(){
    window.recorder.start(1000);
}

function stopRecording(){
    window.recorder.stop();
}

function beSafe(blocker="BLOCK_ONLY_HIGH"){
   return [
        {
            "category": "HARM_CATEGORY_HARASSMENT",
            "threshold": blocker
        },
        {
            "category": "HARM_CATEGORY_HATE_SPEECH",
            "threshold": blocker
        },
        {
            "category": "HARM_CATEGORY_SEXUALLY_EXPLICIT",
            "threshold": blocker
        },
        {
            "category": "HARM_CATEGORY_DANGEROUS_CONTENT",
            "threshold": blocker
        }
        
    ]
}