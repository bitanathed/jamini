const gulp = require('gulp');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const concatCSS = require('gulp-concat-css');
const javascriptObfuscator = require("gulp-javascript-obfuscator");

gulp.task("default", function () {
  return  gulp.src("src/*.js")
      .pipe(concat("index.js"))
      .pipe(javascriptObfuscator({
        compact: true,
        simplify: true,
        unicodeEscapeSequence: true,
        stringArray: false,
        splitStrings: true,
        splitStringsChunkLength: 6
      }))
      .pipe(gulp.dest('public/js'));
});

gulp.task("style", function () {
  return  gulp.src("style/*.css")
      .pipe(concatCSS("index.css"))
      .pipe(cleanCSS())
      .pipe(gulp.dest('public/css'));
});