# JAMini - Generate Entire Songs in Key with just a few seconds of audio
---
Built for Google AI Hackathon. Is a prototype. Uses Gemini 1.5 🤖

A Simple tool to help you generate lyrics to your songs from just a few lines sung (_instruments optional_).

Alter scale, mood, theme of existing songs to create a new one. Or remix your songs to help complete your songs or get over your writer's block.

![JAMini to finish songs](https://ik.imagekit.io/0jpx1foc3/Angel_start%20copy.png?updatedAt=1714671521495 "Record a few lines of a song to have JAMini finish the entire thing in scale and with notation!")

### API Features Used
Some features were experimental. Here's what we used:
- Audio WAVs encoded as Base64 for REST inputs
- System Instructions for JSON Outputs
- Safety Checker High Only (for multilingual output unnerfing)
- Musical Notations as Textual Context (Gemini sometimes messes these up)

### Technologies Used
We kept things simple by using plain old Javascript for our API calls and JSON parsing. That's it, nothing fancy. We converted audio from getUserMedia to base64, added prompts in JSON then used the REST API to dispatch to Gemini. Once we received JSON outputs we used plain JS parsing to convert to HTML and highlight divs.


![Result Outputs](https://ik.imagekit.io/0jpx1foc3/Angel_end%20copy.png?updatedAt=1714671521222 "The final output. Here's what it looks like with JAMini's understanding of music")



